# gemm hardware accelerator

General Matrix Multiply fixed point parametric hardware accelerator tested on FPGA.

The circuit computes:
```
C = alpha * A * B + beta * C
```
where matirces A and B can be additionaly transposed.

# Architecture
The acceleration comes from the fact that the matrix multiplication is on the order of O(n^3) complexity while the necessary readout of matrix elements from memory is on the order of O(n^2). Thus, if every element of the matrix is read only once in to memory and n computations are executed in parallel a speed-up by a factor of n is possible. The operation that is parallelized is the dot product which normally takes O(n) steps to execute sequentially, but when computed by a pipeline tree only one step is needed to get new dot product results.

The algorithm works as follows:
```{python, eval=False}
load n numbers from matrix B into vector b_calc # takes n clock cycles
for i in range(n):
  load n numbers from matrix A into vector a[i] # takes n clock cycles
for _ in range(n):
  for i in range(n):
    calculate dot product a[i] * b_calc and load one number from matrix B into vector b_mem # takes 1 clock cycle
  swap b_calc with b_mem 
```
Where memory 'a' holds the whole matrix A accessible for computation. On the image below the memory 'a' can hold a 4×4 matrix.
The memory for matrix B accessible for computation only holds one vector for computation (b\_calc) and one for RAM readouts (b\_mem).
Once the matrix A vector b\_calc (A*b\_calc) product was computed the vectors b_calc and b_mem are swapped.
The total steps needed to compute a gemm operation for matrices of size n×n takes more or less n + n^2 + 2·n^2 + 6 + log2(n) + 2 cycles.
Where the first n + n^2 comes from loading initial b\_calc vector and whole matrix A.
The 2·n^2 comes from the actual matrix multiplication, it takes twice the actual computation steps because writing the matrix into memory needs one cycle with a stable result from the dot\_product\_tree\_pipe block. It takes 6 steps to decode the instruction and log2(n) + 2 additional steps to fill the dot product pipeline for the first time.
For a matrix of size 16×16 this yields 796 clock cycles. With 100 MHz clock this takes around 8 us.
The circuit is parametric and was tested on 4×4, 8×8 and 16×16 matrices with 16-bit long fixed point numbers.
Additionally, a test was performed where a matrix 16×16 was computed on a 8×8 size coprocessor.

<table align="center">
  <tr>
    <td align="center" width="9999">
    <figure>
    <img src="images/dot_product_tree_pipe.png" align="center" width="900" alt="Dot product pipelined tree multiplication block">
    <figcaption> Dot product pipelined tree multiplication block for a 4×4 matricies with 16-bit fixed point numbers </figcaption>
    </figure>
    </td>
  </tr>
</table>

To test the coprocessor on an FPGA a UART communication block was needed and an additional block that would interpret and execute the commands from the host computer. In a more realistic setting the coprocessor would be used as a sub-circuit in a greater design and the UART and host\_device\_interface blocks would not be used.

Unfortunately I do not own the design for the UART block. The code for this block can be obtained from the book 'FPGA Prototyping by Verilog Examples: Xilinx Spartan-3 Version' by Dr. Pong P. Chu. Or directly from the [Dr. Pong P. Chu website](https://academic.csuohio.edu/chu_p/rtl/fpga_vlog.html). The files used only by the uart block: uart.v, uart\_tx.v, uart\_rx.v, fifo.v and mod\_m\_counter.v need to be replaced by the code from the book or some other implementation.

<table align="center">
  <tr>
    <td align="center" width="9999">
    <figure>
    <img src="images/coprocessor_memory_and_host_interface.png" align="center" width="900" alt="Interface of the coprocessor with the ram memories and the host-device interface">
    </figure>
    </td>
  </tr>
</table>

# Hardware Tests
The design was tested on a FPGA development board NEXYS 4 DDR from Digilent.
The board features a Xilinx Artix-7 xc7a100tcsg324-1 chip.
The test scheme runs as follows:
* Random matrices are generated on the host computer
* The matrices are sent on to the FPGA board via USB-UART interface
* The matrices and the instruction code are stored on the FPGA chip internal RAM
* The host computer sends a 'execute' command to the FPGA to start the computation procedure
* The FPGA computes the gemm result
* the host computer reads the result matrix from the FPGA internal RAM and compares with the result computed on the host computer

# Hardware utilization
On Xilinx Artix-7 xc7a100tcsg324-1 chip the whole circuit with UART controller and host\_device\_interface consumed the following resources:

<table align="center">
  <tr>
    <td align="center" width="9999">
    <figure>
    <img src="images/hardware_utilization.png" align="center" width="900" alt="Hardware utilization of Xilinx Artix-7 xc7a100tcsg324-1 chip">
    </figure>
    </td>
  </tr>
</table>

