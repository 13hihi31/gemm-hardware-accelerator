module my_ram
  #(
  parameter
  ADDR_WIDTH = 12,
  DATA_WIDTH = 8
  )
  (
  input wire clk,
  input wire we,
  input wire [ADDR_WIDTH-1:0] addr,
  input wire [DATA_WIDTH-1:0] din,
  output wire [DATA_WIDTH-1:0] dout
  );
  
  reg [DATA_WIDTH-1:0] ram [2**ADDR_WIDTH-1:0];

  always @(posedge clk)
    if(we)
      ram[addr] <= din;
     
  assign dout = ram[addr];
   
endmodule