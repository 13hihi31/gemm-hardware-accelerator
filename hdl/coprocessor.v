`timescale 1ns / 1ps

module coprocessor
  #(
  parameter
  ADDR_WIDTH = 12,
  DATA_WIDTH = 16,
  MATRIX_SIZE = 8
  )
  (
  input wire clk, reset,

  // master interface
  input wire execute,
  input wire [ADDR_WIDTH-1:0] addr_intr,
  output wire error,
  output reg done,

  // memory a interface
  output reg [ADDR_WIDTH-1:0] addr_a,
  input wire [DATA_WIDTH-1:0] dout_a,

  // memory b interface
  output reg [ADDR_WIDTH-1:0] addr_b,
  output wire [DATA_WIDTH-1:0] din_b,
  input wire [DATA_WIDTH-1:0] dout_b,
  output reg we_b,

  // debug signals
  output wire [3:0] state
  );

  localparam LOG2_MAT_SIZE = $clog2(MATRIX_SIZE);
  // matrix is MATRIX_SIZE * MATRIX_SIZE thus a vector made of matrix column vectors
  // is log2(MATRIX_SIZE^2) = 2 * log2(MATRIX_SIZE)
  localparam MAT_OFFSET_SIZE = 2 * LOG2_MAT_SIZE;

  localparam PIPE_DEPTH = 1 + LOG2_MAT_SIZE + 1;
  localparam LOG2_PIPE_DEPTH = $clog2(PIPE_DEPTH);

  // instruction coding
  // matrix multiplication has a 1'b1 on the LSB
  // the second bit tells if matrix B is transposed, 1 if it is
  // the third bit tells if matrix A is transposed, 1 if it is
  // the rest of bits can be used to code for different operations

  // BRAM memory accessing note:
  // because it takes one clock cycle from specifing the adress to
  // getting the valid data, we have to specify the address one state earlier

  localparam [3:0]
    idle = 0,
    load_reg1 = 1,
    load_reg2 = 2,
    load_reg3 = 3,
    load_reg4 = 4,
    load_reg5 = 5,
    load_first_B_vec = 6,
    load_A_mat = 7,
    calc_and_load_B = 8,
    wait_for_C_0 = 9,
    wait_for_C_1 = 10,
    drain_pipe = 11,
    done_state = 12;
  
  reg [3:0] state_reg, state_next;
  reg [DATA_WIDTH-1:0] reg0_reg, reg0_next; // stores the instruction code
  reg [DATA_WIDTH-1:0] reg1_reg, reg1_next; // stores the address of operand A
  reg [DATA_WIDTH-1:0] reg2_reg, reg2_next; // stores the address of operand B
  reg [DATA_WIDTH-1:0] reg3_reg, reg3_next; // stores the address of operand C or junk
  reg [DATA_WIDTH-1:0] reg4_reg, reg4_next; // stores the value alpha or length of arguments 
  reg [DATA_WIDTH-1:0] reg5_reg, reg5_next; // stores the value beta or junk
  reg [ADDR_WIDTH-1:0] instr_addr_reg, instr_addr_next;

  // signals for memory A holding the matrix A
  reg [LOG2_MAT_SIZE-1:0] mem_A_addr_reg, mem_A_addr_next;
  reg mem_A_we;

  // signals for memory B holding some of the vectors from matrix B
  reg mem_B_swap_vec;
  reg mem_B_reset;
  reg mem_B_we;

  // counter for keeping track how many elements of a vector or vectors from matrix have been processed
  reg [LOG2_MAT_SIZE-1:0] n_reg, n_next;
  wire n_reg_equal_zero;

  // signals for controling the accessing of matrix A, B and C elements from main memory
  reg [MAT_OFFSET_SIZE-1:0] mat_A_offset_reg, mat_A_offset_next; // pointer to matrix A element relative to the matrix A address in main memory
  reg [MAT_OFFSET_SIZE-1:0] mat_B_offset_reg, mat_B_offset_next; // pointer to matrix B element relative to the matrix B address in main memory
  reg [MAT_OFFSET_SIZE-1:0] mat_C_offset_reg, mat_C_offset_next; // pointer to matrix C element relative to the matrix C address in main memory
  wire transpose_mat_A; // 1 if matrix A should be transposed
  wire transpose_mat_B; // 1 if matrix B should be transposed
  wire [MAT_OFFSET_SIZE-1:0] mat_A_offset_plus_inc; // we jump over matrix A elements differently if we take its transpose
  wire [MAT_OFFSET_SIZE-1:0] mat_B_offset_plus_inc; // we jump over matrix B elements differently if we take its transpose

  reg [LOG2_MAT_SIZE-1:0] vec_C_wr_counter_reg, vec_C_wr_counter_next; // counts the number of elements writen of a column vector from matrix C

  reg error_reg, error_next;

  // signals for controling the pipeline valid-data depth counter
  reg pipe_depth_inc, pipe_depth_dec, pipe_depth_reset;
  wire pipe_full, pipe_empty;
  reg pipe_we;

  wire [2*DATA_WIDTH-1:0] product;
  reg [DATA_WIDTH-1:0] alpha_result_reg;
  wire [DATA_WIDTH-1:0] alpha_result_next;
  wire [2*DATA_WIDTH-1:0] beta_dout_b;
  
  // registers with asynchronous reset
  always @(posedge clk, posedge reset)
    if(reset)
      state_reg <= idle;
    else
      state_reg <= state_next;

  // registers
  always @(posedge clk) begin
    reg0_reg <= reg0_next;
    reg1_reg <= reg1_next;
    reg2_reg <= reg2_next;
    reg3_reg <= reg3_next;
    reg4_reg <= reg4_next;
    reg5_reg <= reg5_next;
    instr_addr_reg <= instr_addr_next;
    mem_A_addr_reg <= mem_A_addr_next;
    n_reg <= n_next;
    mat_A_offset_reg <= mat_A_offset_next;
    mat_B_offset_reg <= mat_B_offset_next;
    mat_C_offset_reg <= mat_C_offset_next;
    vec_C_wr_counter_reg <= vec_C_wr_counter_next;
    error_reg <= error_next;
    alpha_result_reg <= alpha_result_next;
  end
  
  // assign intermediate signals
  assign n_reg_equal_zero = (n_reg == 0);
  assign transpose_mat_A = reg0_reg[2]; // if instruction code has third bit on - transpose matrix A
  assign transpose_mat_B = reg0_reg[1]; // if instruction code has second bit on - transpose matrix B
  assign mat_A_offset_plus_inc = transpose_mat_A ? (mat_A_offset_reg + MATRIX_SIZE) : (mat_A_offset_reg + 1);
  assign mat_B_offset_plus_inc = transpose_mat_B ? (mat_B_offset_reg + 1) : (mat_B_offset_reg + MATRIX_SIZE);

  // assign outputs
  assign error = error_reg;

  reg [3:0] last_state_reg;
  wire [3:0] last_state_next;

  // assign debug signals
  //assign state = state_reg;
  assign state = last_state_reg;
  assign last_state_next = (state_next == idle) ? last_state_reg : state_reg; 
  
  always @(posedge clk) 
    last_state_reg <= last_state_next;
  
  always @* begin
    // default next states and control signals
    state_next = state_reg;
    reg0_next = reg0_reg;
    reg1_next = reg1_reg;
    reg2_next = reg2_reg;
    reg3_next = reg3_reg;
    reg4_next = reg4_reg;
    reg5_next = reg5_reg;
    instr_addr_next = instr_addr_reg + 1;

    mem_A_addr_next = mem_A_addr_reg;
    mem_A_we = 1'b0;

    mem_B_swap_vec = 1'b0;
    mem_B_reset = 1'b0;
    mem_B_we = 1'b0;

    n_next = n_reg;

    mat_A_offset_next = mat_A_offset_reg;
    mat_B_offset_next = mat_B_offset_reg;
    mat_C_offset_next = mat_C_offset_reg;

    vec_C_wr_counter_next = vec_C_wr_counter_reg;

    error_next = error_reg;

    pipe_depth_dec = 1'b0;
    pipe_depth_inc = 1'b0;
    pipe_depth_reset = 1'b0;
    pipe_we = 1'b0;

    // default outputs
    done = 1'b0;
    // default address a for reading matrix A elements
    addr_a = reg1_reg + mat_A_offset_reg;
    // default address b for reading matrix C elements
    addr_b = reg3_reg + mat_C_offset_reg;
    we_b = 1'b0;

    // next state logic
    case(state_reg)
      idle: begin
        if(execute == 1'b1) begin
          reg0_next = dout_a;
          addr_a = addr_intr + 1; // address for the next one (for reg1)
          instr_addr_next = addr_intr + 2; // address for reg2
          state_next = load_reg1;
        end
        pipe_depth_reset = 1'b1;
        // keep the memories for matrices A, B and C ready
        mem_A_addr_next = 0;
        mem_B_reset = 1'b1;
        mat_A_offset_next = 0;
        mat_B_offset_next = 0;
        mat_C_offset_next = 0;
        vec_C_wr_counter_next = 0;
      end
      load_reg1: begin
        reg1_next = dout_a;
        addr_a = instr_addr_reg;
        state_next = load_reg2;
      end
      load_reg2: begin
        reg2_next = dout_a;
        addr_a = instr_addr_reg;
        state_next = load_reg3;
      end
      load_reg3: begin
        reg3_next = dout_a;
        addr_a = instr_addr_reg;
        state_next = load_reg4;
      end
      load_reg4: begin
        reg4_next = dout_a;
        addr_a = instr_addr_reg;
        state_next = load_reg5;
      end
      load_reg5: begin
        reg5_next = dout_a;
        
        if(reg0_reg[0]) begin // matrix multiplication
          state_next = load_first_B_vec;
          n_next = {LOG2_MAT_SIZE{1'b1}};
          
          // load from matrix B first element for the next cycle
          addr_a = reg2_reg + mat_B_offset_reg;
          // update matrix B address in physical memory
          mat_B_offset_next = mat_B_offset_plus_inc;
        end
        else begin
          // fatal error - given code is unknown
          error_next = 1'b1;
          state_next = done_state;
        end
      end
      load_first_B_vec: begin
        // write to mem B current data
        mem_B_we = 1'b1;
        // we just finished reading a vector element from matrix B
        // here n serves as the counter of vector elements
        n_next = n_reg - 1;
        // if the whole vector was read
        if(n_reg_equal_zero) begin
          // n is a power of two the counter will flip over by itself
          //n_next = {LOG2_MAT_SIZE{1'b1}};
          state_next = load_A_mat;
          // load from matrix A for the next cycle new data
          addr_a = reg1_reg + mat_A_offset_reg;
          // update matrix A address in physical memory
          mat_A_offset_next = mat_A_offset_plus_inc;
          // we have loaded a whole vector from matrix B - we can swap the vectors
          mem_B_swap_vec = 1'b1;
          
          // if matrix B is not transposed we need to increment the current pointer by one
          // because otherwise incrementing the offset by matrix size will filp it to the starting offset value
          if(!transpose_mat_B) begin
            mat_B_offset_next = mat_B_offset_reg + 1;
          end
        end
        else begin
          // load from matrix B for the next cycle new data
          addr_a = reg2_reg + mat_B_offset_reg;
          // update matrix B address in physical memory
          mat_B_offset_next = mat_B_offset_plus_inc;
        end
      end
      load_A_mat: begin
        // load from matrix A for the next cycle new data
        // this is the default addr_a output so it is commented out
        //addr_a = reg1_reg + mat_A_offset_reg;
        // write to mem A current data
        mem_A_we = 1'b1;
        
        // we just finished reading a vector element from matrix A
        // here n serves as the counter of writen vectors
        n_next = n_reg - 1;
        // if the whole vector of matrix A was read
        if(n_reg_equal_zero) begin
          // update the mem A address - we just finished writing one row vector
          mem_A_addr_next = mem_A_addr_reg + 1;
          // if the whole matrix was read
          if(mem_A_addr_next == 0) begin
            // we dont need to wait for matrix C elements just yet
            // because the data didn't propagate through the pipeline
            // but we need to load matirx B elements
            state_next = wait_for_C_0;
          end
          // n is a power of two the counter will flip over by itself
          //n_next = {LOG2_MAT_SIZE{1'b1}};
        end
        
        // if matrix A is transposed and we will have read the whole vector by the next cycle we need to increment the current pointer by one
        // because otherwise incrementing the offset by matrix size will filp it to the starting offset value
        if((n_next == 0) && transpose_mat_A) begin
          mat_A_offset_next = mat_A_offset_plus_inc + 1;
        end
        else begin
          // update matrix A address in physical memory
          mat_A_offset_next = mat_A_offset_plus_inc;
        end
      end
      calc_and_load_B: begin
        // write to mem B current data
        mem_B_we = 1'b1;
        
        // update the mem A address - we just finished computing the dot product
        mem_A_addr_next = mem_A_addr_reg + 1;
        
        // push the data through the pipe
        pipe_we = 1'b1;
        pipe_depth_inc = 1'b1;
        
        // if we have multiplied current B vector by every row of matrix A
        if(mem_A_addr_next == 0) begin
          // change to just loaded B vector
          mem_B_swap_vec = 1'b1;
          
          // if matrix B is not transposed we need to increment the current pointer by one
          // because otherwise incrementing the offset by matrix size will filp it to the starting offset value
          if(!transpose_mat_B) begin
            mat_B_offset_next = mat_B_offset_reg + 1;
          end
          
          // here n serves as the counter of matrix A times vector B multiplications
          n_next = n_reg - 1;
        end
        
        // if we have multiplied every row vector of matrix A by every column vector of matrix B 
        if(mem_A_addr_next == 0 && n_reg_equal_zero) begin
          state_next = wait_for_C_1;
        end
        else begin
          // we have writen data in this step (in the 'if' statement below)
          // we need to wait one cycle to read a new value of C
          // wait for mat_C_offset_reg to update and thus addr_b to update and point at the next address
          state_next = wait_for_C_0;
        end
       
        // note: the pipeline will be full when we have multiplied by numbers
        // thus the if statement below will happen before entering wait_for_C_1
       
        if(pipe_full) begin
          // the data has propagated through the whole pipelined tree
          // we can write the result to physical memory
          we_b = 1'b1;
          
          // update the counter of writen elements
          vec_C_wr_counter_next = vec_C_wr_counter_reg + 1;
          
          // note: the data on the output of dout_b will be valid
          // because addr_b has a default assingment that happend
          // one cycle earlier in wait_for_C_0 state
          
          // update matrix C address in physical memory
          // note: if we have finished computing the column vector of matrix C
          // we must add one to the offset so that we access the first element of the next column
          
          if(vec_C_wr_counter_next == 0) begin
            mat_C_offset_next = mat_C_offset_reg + MATRIX_SIZE + 1;
          end
          else begin
            mat_C_offset_next = mat_C_offset_reg + MATRIX_SIZE;
          end
          
          //mat_C_offset_next = mat_C_offset_reg + 1;
        end
      end
      wait_for_C_0: begin
        // load from matrix B for the next cycle new data
        addr_a = reg2_reg + mat_B_offset_reg;
        // update matrix B address in physical memory
        mat_B_offset_next = mat_B_offset_plus_inc;
        
        // load from matrix C for the next cycle new data for reading and then writing
        // this is the default addr_b output so it is commented out
        //addr_b = reg3_reg + mat_C_offset_reg;
        state_next = calc_and_load_B;
      end
      wait_for_C_1: begin
        // load from matrix C for the next cycle new data
        // this is the default addr_b output so it is commented out
        //addr_b = reg3_reg + mat_C_offset_reg;
        state_next = drain_pipe;
      end
      drain_pipe: begin  
        if(pipe_empty) begin
          state_next = done_state;
        end
        else begin
          // the data has propagated through the whole pipelined tree
          // we can write the result to physical memory
          we_b = 1'b1;
          // note: the data on the output of dout_b will be valid
          // because addr_b has a default assingment that happend
          // one cycle earlier in wait_for_C_1 state
        
          // we have writen data in this step
          // we need to wait one cycle to read a new value 
          // load from matrix C for the next cycle new data
          // this is the default addr_b output so it is commented out
          //addr_b = reg3_reg + mat_C_offset_reg;
          state_next = wait_for_C_1;
        end
        
        // we drain one valid result from the pipeline
        pipe_depth_dec = 1'b1;
        // push the data through the pipe
        pipe_we = 1'b1;
        
        // update matrix C address in physical memory
        // here we assume that the pipeline depth is shorter than the matrix size
        // if this is true we dont need to add extra one to point at a new column
        // because mat_C_offset will already point at the last column
        mat_C_offset_next = mat_C_offset_reg + MATRIX_SIZE;
        //mat_C_offset_next = mat_C_offset_reg + 1;
      end
      done_state: begin
        error_next = 1'b0;
        done = 1'b1;
        state_next = idle;
      end
    endcase
  end
  
  saturated_counter #(.MAX_VALUE(PIPE_DEPTH), .N_BITS(LOG2_PIPE_DEPTH+1)) pipe_depth_counter
    (.clk(clk), .reset(pipe_depth_reset),
    .dec(pipe_depth_dec), .inc(pipe_depth_inc), .y(),
    .min(pipe_empty), .max(pipe_full));
  
  genvar i;
  
  // mem B - used to store the vectors of matrix B
  wire [0:MATRIX_SIZE*DATA_WIDTH-1] mem_B_dout_vec;
  //wire [DATA_WIDTH-1:0] mem_B_dout [0:MATRIX_SIZE-1];
  
  two_vector_mem #(.DATA_WIDTH(DATA_WIDTH), .VECTOR_LENGTH(MATRIX_SIZE)) mem_B
   (.clk(clk), .reset(reset), .we(mem_B_we), .swap_vec(mem_B_swap_vec),
   .reset_vec(mem_B_reset), .din(dout_a), .dout(mem_B_dout_vec));
  
  /*
  generate
  for(i = 0; i < MATRIX_SIZE; i = i + 1) begin
   assign mem_B_dout[i] = mem_B_dout_vec[i*DATA_WIDTH:(i+1)*DATA_WIDTH-1];
  end
  endgenerate
  */
  
  // end of mem B
  
  // mem A - used to store the vectors of matrix A 
  wire [0:MATRIX_SIZE*DATA_WIDTH-1] mem_A_dout_vec;
  wire [DATA_WIDTH-1:0] mem_A_dout [0:MATRIX_SIZE-1];
  
  my_ram #(.ADDR_WIDTH(LOG2_MAT_SIZE), .DATA_WIDTH(DATA_WIDTH)) my_ram_unit_0
   (.clk(clk), .we(mem_A_we), .addr(mem_A_addr_reg), .din(dout_a), .dout(mem_A_dout[0]));
  
  generate
  for(i = 1; i < MATRIX_SIZE; i = i + 1) begin : my_ram_unit
   my_ram #(.ADDR_WIDTH(LOG2_MAT_SIZE), .DATA_WIDTH(DATA_WIDTH))
     (.clk(clk), .we(mem_A_we), .addr(mem_A_addr_reg), .din(mem_A_dout[i-1]), .dout(mem_A_dout[i]));
  end
  endgenerate
  
  // pack the array in to a vector
  generate
  for(i = 0; i < MATRIX_SIZE; i = i + 1) begin
    assign mem_A_dout_vec[i*DATA_WIDTH:(i+1)*DATA_WIDTH-1] = mem_A_dout[i];
  end
  endgenerate
  // end of mem A
  
  // extended dot product with alpha multiplication and beta times matrix C element addition
  wire [2*DATA_WIDTH+LOG2_MAT_SIZE-1:0] dot_product_result;
  
  dot_product_tree_pipe #(.DATA_WIDTH(DATA_WIDTH), .LENGTH(MATRIX_SIZE), .LOG2_LENGTH(LOG2_MAT_SIZE)) dot_product_unit
   (.clk(clk), .we(pipe_we), .opA(mem_A_dout_vec), .opB(mem_B_dout_vec), .result(dot_product_result));
   
  assign product = dot_product_result[DATA_WIDTH-1:0] * reg4_reg;
  assign alpha_result_next = (pipe_we == 1'b1) ? product[DATA_WIDTH-1:0] : alpha_result_reg;
  
  assign beta_dout_b = reg5_reg * dout_b;
  assign din_b = alpha_result_reg + beta_dout_b[DATA_WIDTH-1:0];
  //assign din_b = alpha_result_reg;
  
endmodule
