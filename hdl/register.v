`timescale 1ns / 1ps

module register
  #(
  parameter
  DATA_WIDTH = 16
  )
  (
  input wire clk, reset,
  input wire we,
  input wire [DATA_WIDTH-1:0] din,
  output wire [DATA_WIDTH-1:0] dout
  );

  reg [DATA_WIDTH-1:0] d_reg;
  wire [DATA_WIDTH-1:0] d_next;
  
  assign d_next = (we == 1'b1) ? din : d_reg; 
  assign dout = d_reg;
  
  always @(posedge clk, posedge reset)
    if(reset)
      d_reg <= {DATA_WIDTH{1'b0}};
    else
      d_reg <= d_next;
      
endmodule