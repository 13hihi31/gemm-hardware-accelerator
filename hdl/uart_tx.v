
// Sorry for the inconvenience, I do not own the design of this block.
// The code can be obtained from the book 'FPGA Prototyping by Verilog Examples: Xilinx Spartan-3 Version' by Dr. Pong P. Chu
// or directly from Dr. Pong P. Chu website:
//   https://academic.csuohio.edu/chu_p/rtl/fpga_vlog.html
//   https://academic.csuohio.edu/chu_p/rtl/fpga_vlog_book/fpga_vlog_src.zip
// replace this file with the file from Dr. Pong P. Chu code listing:
//   list_ch08_03_uart_tx.v

