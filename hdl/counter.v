`timescale 1ns / 1ps

module counter
  #(parameter N=10)
  (
  input wire clk, reset,
  input wire restart,
  output wire tic
  );
  
  reg [N-1:0] counter_reg;
  wire [N-1:0] counter_next;
  
  // registers
  always @(posedge clk, posedge reset)
    if(reset)
      counter_reg <= {N{1'b1}};
    else
      counter_reg <= counter_next;
  
  // next state logic
  assign counter_next = restart ? {N{1'b1}} : counter_reg - 1'b1;
  
  // output logic
  assign tic = (counter_reg == {N{1'b0}}) ? 1'b1 : 1'b0;
  
endmodule