`timescale 1ns / 1ps

module host_device_interface
  #(
  parameter 
  ADDR_WIDTH = 12,
  DATA_WIDTH = 16,
  CODE_WIDTH = 8
  )
  (
  input wire clk, reset,
  
  // receiver-transmiter interface
  input wire new_rx_data,
  input wire [7:0] rx_data,
  output reg rd_rx_data,
  input wire tx_busy,
  output reg new_tx_data,
  output reg [7:0] tx_data,
  
  // memory interface
  output wire [ADDR_WIDTH-1:0] addr,
  output wire [DATA_WIDTH-1:0] din,
  
  // memory a interface
  output wire we_a,
  input wire [DATA_WIDTH-1:0] dout_a,
  
  // memory b interface
  output wire we_b,
  input wire [DATA_WIDTH-1:0] dout_b,
  
  // grant memory access
  output reg source,
  
  // coprocessor interface
  input wire done,
  output reg cop_execute
  );
  
  localparam [2:0]
    rx_0 = 3'b000,
    rx_1 = 3'b001,
    rx_2 = 3'b010,
    rx_3 = 3'b011,
    read = 3'b100,
    write = 3'b101,
    execute = 3'b110;
  
  // the first bit of the received code
  // tells if the memory a or memory b should be acessed,
  // if it is 1 than memory b is acessed
  localparam [CODE_WIDTH-2:0]
    WRITE_CODE = 0,
    READ_CODE = 1,
    EXECUTE_CODE = 2;
  
  localparam BYTE_WIDTH = 8;
  localparam BYTE_INDEX_WIDTH = $clog2(DATA_WIDTH / BYTE_WIDTH);
  
  // regs
  reg [2:0] state_reg, state_next;
  reg [BYTE_WIDTH-1:0] instr_reg, instr_next; // holds the instruction code
  reg [ADDR_WIDTH-1:0] addr_reg, addr_next; // holds the memory address
  reg [BYTE_WIDTH-1:0] num_reg, num_next; // holds a byte with the number of data transfered
  reg [BYTE_INDEX_WIDTH-1:0] byte_index_reg, byte_index_next; // holds the index of byte in the memory entry
  reg [BYTE_WIDTH-1:0] mem_lsb_0_din_reg, mem_lsb_0_din_next; // the memory input byte idx 0 (LSB)
  // the MSB byte is not registered, it is directly taken from the rx input 
  //reg [BYTE_WIDTH-1:0] mem_lsb_1_din_reg, mem_lsb_1_din_next; // the memory input byte idx 1 (MSB)
  
  wire [ADDR_WIDTH+BYTE_INDEX_WIDTH-1:0] byte_address_plus_1;
  wire [BYTE_WIDTH-1:0] num_reg_minus_1;
  wire num_reg_minus_1_eq_0;
  reg timeout_restart;
  wire timeout_tic;
  
  wire [DATA_WIDTH-1:0] dout;
  reg we;
  
  // registers with asynchronous reset
  always @(posedge clk, posedge reset)
    if(reset) begin
      state_reg <= rx_0;
    end
    else begin
      state_reg <= state_next;
    end
  
  // registers
  always @(posedge clk) begin
   instr_reg <= instr_next;
   addr_reg <= addr_next;
   num_reg <= num_next;
   byte_index_reg <= byte_index_next;
   mem_lsb_0_din_reg <= mem_lsb_0_din_next;
  end
  
  assign byte_address_plus_1 = {addr_reg, byte_index_reg} + 1'b1;
  assign num_reg_minus_1 = num_reg - 1'b1;
  assign num_reg_minus_1_eq_0 = (num_reg_minus_1 == 0);
  
  assign addr = addr_reg;
  assign din = {rx_data, mem_lsb_0_din_reg};
  assign dout = (instr_reg[0] == 1'b1) ? dout_b : dout_a; 
  assign we_a = (instr_reg[0] == 1'b1) ? 1'b0 : we;
  assign we_b = (instr_reg[0] == 1'b1) ? we : 1'b0;
  
  // next-state logic
  always @* begin
    // default next states
    state_next = state_reg;
    instr_next = instr_reg;
    addr_next = addr_reg;
    num_next = num_reg;
    byte_index_next = byte_index_reg;
    mem_lsb_0_din_next = mem_lsb_0_din_reg;
    
    // default outputs and control signals
    rd_rx_data = 1'b0;
    new_tx_data = 1'b0;
    tx_data = dout[BYTE_WIDTH-1:0];
    we = 1'b0;
    source = 1'b0;
    timeout_restart = 1'b0;
    cop_execute = 1'b0;
    
    case(state_reg)
      rx_0:
        if(new_rx_data) begin
          instr_next = rx_data;
          rd_rx_data = 1'b1;
          state_next = rx_1;
          timeout_restart = 1'b1;
        end
      rx_1:
        if(new_rx_data) begin
          addr_next[BYTE_WIDTH-1:0] = rx_data;
          rd_rx_data = 1'b1;
          state_next = rx_2;
          timeout_restart = 1'b1;
        end
        else if(timeout_tic) begin
          state_next = rx_0;
        end
      rx_2:
        if(new_rx_data) begin
          addr_next[ADDR_WIDTH-1:BYTE_WIDTH] = rx_data[ADDR_WIDTH-BYTE_WIDTH-1:0];
          rd_rx_data = 1'b1;
          state_next = rx_3;
          timeout_restart = 1'b1;
        end
        else if(timeout_tic) begin
          state_next = rx_0;
        end
      rx_3:
        if(new_rx_data) begin
          num_next = rx_data;
          rd_rx_data = 1'b1;
          if(instr_reg[BYTE_WIDTH-1:1] == READ_CODE)
            state_next = read;
          else if(instr_reg[BYTE_WIDTH-1:1] == WRITE_CODE)
            state_next = write;
          else if(instr_reg[BYTE_WIDTH-1:1] == EXECUTE_CODE)
            state_next = execute;
          else
            state_next = rx_0;
          byte_index_next = 1'b0;
          timeout_restart = 1'b1;
        end
        else if(timeout_tic) begin
          state_next = rx_0;
        end
      read:
        if(!tx_busy) begin
          case(byte_index_reg)
            1'b0:
              tx_data = dout[BYTE_WIDTH-1:0];
            1'b1:
              tx_data = dout[DATA_WIDTH-1:BYTE_WIDTH];
          endcase
          
          new_tx_data = 1'b1;
          num_next = num_reg_minus_1;
          timeout_restart = 1'b1;
          
          byte_index_next = byte_address_plus_1[BYTE_INDEX_WIDTH-1:0];
          addr_next = byte_address_plus_1[ADDR_WIDTH+BYTE_INDEX_WIDTH-1:BYTE_INDEX_WIDTH];
          
          if(num_reg_minus_1_eq_0)
            state_next = rx_0;
        end
        else if(timeout_tic) begin
          state_next = rx_0;
        end
      write:
        if(new_rx_data) begin
          case(byte_index_reg)
            1'b0:
              mem_lsb_0_din_next = rx_data;
            1'b1:
              we = 1'b1;
          endcase
          
          rd_rx_data = 1'b1;
          num_next = num_reg_minus_1;
          timeout_restart = 1'b1;
          
          byte_index_next = byte_address_plus_1[BYTE_INDEX_WIDTH-1:0];
          addr_next = byte_address_plus_1[ADDR_WIDTH+BYTE_INDEX_WIDTH-1:BYTE_INDEX_WIDTH];
          
          if(num_reg_minus_1_eq_0)
            state_next = rx_0;
        end
        else if(timeout_tic) begin
          state_next = rx_0;
        end
      execute: begin
       cop_execute = 1'b1;
       
       // give memory axcess to coprocessor
       source = 1'b1;
        
       if(done)
         state_next = rx_0;
      end
    endcase
  end
    
  counter
  #(.N(20))
  timeout_counter
  (
  .clk(clk),
  .reset(reset),
  .restart(timeout_restart),
  .tic(timeout_tic)
  );
  
endmodule