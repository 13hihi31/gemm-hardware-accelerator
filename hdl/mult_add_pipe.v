`timescale 1ns / 1ps

module mult_add_pipe
  #(
  parameter
  DATA_WIDTH = 16
  )
  (
  input wire clk, we,
  input wire [DATA_WIDTH-1:0] a0,
  input wire [DATA_WIDTH-1:0] a1,
  input wire [DATA_WIDTH-1:0] b0,
  input wire [DATA_WIDTH-1:0] b1,
  output wire [2*DATA_WIDTH:0] y
  );
  
  reg [2*DATA_WIDTH-1:0] a0b0_reg, a0b0_next;
  reg [2*DATA_WIDTH-1:0] a1b1_reg, a1b1_next;
  reg [2*DATA_WIDTH:0] y_reg, y_next;
  
  always @(posedge clk) begin
    if(we) begin
      a0b0_reg <= a0b0_next;
      a1b1_reg <= a1b1_next;
      y_reg <= y_next;
    end
  end

  always @* begin
    a0b0_next = a0 * b0;
    a1b1_next = a1 * b1;
    y_next = a0b0_reg + a1b1_reg;
  end
  
  assign y = y_reg;
 
endmodule