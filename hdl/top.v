`timescale 1ns / 1ps

module top(
  input wire CLK100MHZ,
  input wire BTNU,
  input wire rx,
  output wire tx,
  output wire [3:0] LED
  );
    
  wire reset;
  
  wire wr_uart, tx_full;
  wire rd_uart, rx_empty;
  wire [7:0] r_data, w_data;
  
  wire we_a, we_b;
  wire we_a_intr, we_b_intr;
  wire we_b_cop;
  wire [11:0] addr_a, addr_b;
  wire [11:0] addr_ab_intr;
  wire [11:0] addr_a_cop, addr_b_cop;
  wire [15:0] din_a, din_b;
  wire [15:0] din_ab_intr;
  wire [15:0] din_b_cop;
  wire [15:0] dout_a, dout_b;
  wire source;
 
  wire execute, done;
   
  // debug signals
  wire [3:0] state;
   
  assign reset = BTNU;
  
  assign LED = state;
  
  host_device_interface #(.ADDR_WIDTH(12), .DATA_WIDTH(16), .CODE_WIDTH(8)) interface_unit
    (.clk(CLK100MHZ), .reset(reset), 
     .new_rx_data(~rx_empty), .rx_data(r_data), .rd_rx_data(rd_uart), 
     .tx_busy(tx_full), .new_tx_data(wr_uart), .tx_data(w_data), 
     .addr(addr_ab_intr), .din(din_ab_intr), 
     .we_a(we_a_intr), .dout_a(dout_a),
     .we_b(we_b_intr), .dout_b(dout_b),
     .source(source), .done(done), .cop_execute(execute));
  
  // use DVSR 651 and DVSR_BIT 10 for 9600 boud rate
  // use DVSR 54 and DVSR_BIT 6 for 115200 boud rate
  uart #(.DVSR(651), .DVSR_BIT(10), .FIFO_W(2)) uart_unit
    (.clk(CLK100MHZ), .reset(reset), .rd_uart(rd_uart),
    .wr_uart(wr_uart), .rx(rx), .w_data(w_data), .tx_full(tx_full), 
    .rx_empty(rx_empty), .tx(tx), .r_data(r_data));
  
  coprocessor #(.ADDR_WIDTH(12), .DATA_WIDTH(16), .MATRIX_SIZE(8)) coprocessor_unit
    (.clk(CLK100MHZ), .reset(reset), 
     .execute(execute), .addr_intr(addr_ab_intr), .error(), .done(done),
     .addr_a(addr_a_cop), .dout_a(dout_a), 
     .addr_b(addr_b_cop), .din_b(din_b_cop), .dout_b(dout_b), .we_b(we_b_cop),
     .state(state));
  
  assign we_a = we_a_intr;
  assign we_b = source ? we_b_cop : we_b_intr;
  assign addr_a = source ? addr_a_cop : addr_ab_intr;
  assign addr_b = source ? addr_b_cop : addr_ab_intr;
  assign din_a = din_ab_intr;
  assign din_b = source ? din_b_cop : din_ab_intr;
  
  ram ram_a
    (.clk(CLK100MHZ), .reset(reset), .addr(addr_a), .din(din_a), .dout(dout_a), .we(we_a));
    
  ram ram_b
    (.clk(CLK100MHZ), .reset(reset), .addr(addr_b), .din(din_b), .dout(dout_b), .we(we_b));
    
endmodule