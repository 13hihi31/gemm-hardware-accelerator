`timescale 1ns / 1ps

module add_pipe
  #(
  parameter
  DATA_WIDTH = 16
  )
  (
  input wire clk, we,
  input wire [DATA_WIDTH-1:0] a,
  input wire [DATA_WIDTH-1:0] b,
  output reg [DATA_WIDTH:0] c
  );
  
  always @(posedge clk) begin
    if(we) begin
      c <= a + b;
    end
  end
  
endmodule