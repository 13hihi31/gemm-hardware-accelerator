`timescale 1ns / 1ps

module two_vector_mem
  #(
  parameter
  DATA_WIDTH = 16,
  VECTOR_LENGTH = 8
  )
  (
  input wire clk, reset,
  input wire we, swap_vec, reset_vec,
  input wire [DATA_WIDTH-1:0] din,
  output wire [0:VECTOR_LENGTH*DATA_WIDTH-1] dout
  );
  
  reg vec_sel_reg, vec_sel_next;
  wire we_vec0, we_vec1;
  wire [DATA_WIDTH-1:0] vec0 [0:VECTOR_LENGTH-1];
  wire [DATA_WIDTH-1:0] vec1 [0:VECTOR_LENGTH-1];
  wire [DATA_WIDTH-1:0] vec_out [0:VECTOR_LENGTH-1];
  
  assign we_vec0 = we && vec_sel_reg;
  assign we_vec1 = we && ~vec_sel_reg;
  
  register #(.DATA_WIDTH(DATA_WIDTH)) reg0_unit_0
      (.clk(clk), .reset(reset), .we(we_vec0), .din(din), .dout(vec0[0]));
        
  register #(.DATA_WIDTH(DATA_WIDTH)) reg1_unit_0
      (.clk(clk), .reset(reset), .we(we_vec1), .din(din), .dout(vec1[0]));
  
  genvar i;
  
  generate
  for(i = 1; i < VECTOR_LENGTH; i = i + 1) begin : reg0_unit
    register #(.DATA_WIDTH(DATA_WIDTH))
      (.clk(clk), .reset(reset), .we(we_vec0), .din(vec0[i-1]), .dout(vec0[i]));
  end    
  endgenerate
  
  generate
  for(i = 1; i < VECTOR_LENGTH; i = i + 1) begin : reg1_unit
    register #(.DATA_WIDTH(DATA_WIDTH))
      (.clk(clk), .reset(reset), .we(we_vec1), .din(vec1[i-1]), .dout(vec1[i]));
  end
  endgenerate  
  
  generate
  for(i = 0; i < VECTOR_LENGTH; i = i + 1) begin
    assign vec_out[i] = (vec_sel_reg == 1'b0) ? vec0[i] : vec1[i];
  end 
  endgenerate
  
  generate
  for(i = 0; i < VECTOR_LENGTH; i = i + 1) begin
    assign dout[i*DATA_WIDTH:(i+1)*DATA_WIDTH-1] = vec_out[i];
  end
  endgenerate
  
  always @(posedge clk, posedge reset)
    if(reset) begin
      vec_sel_reg <= 1'b0;
    end
    else begin
      vec_sel_reg <= vec_sel_next;
    end
  
  always @* begin
    vec_sel_next = vec_sel_reg;
    if(reset_vec == 1'b1)
      vec_sel_next = 1'b0;
    else if(swap_vec == 1'b1)
      vec_sel_next = ~vec_sel_reg;
  end
endmodule