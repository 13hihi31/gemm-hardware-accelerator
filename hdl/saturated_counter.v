`timescale 1ns / 1ps

module saturated_counter
  #(
  parameter
  MAX_VALUE = 3,
  N_BITS = 2
  )
  (
  input wire clk, reset,
  input wire dec, inc,
  output wire [N_BITS-1:0] y,
  output wire min, max
  );
  
  // this is a counter that can only assign values between 0 and MAX_VALUE
  
  reg [N_BITS-1:0] counter_reg, counter_next;
  
  assign max = (counter_reg == MAX_VALUE);
  assign min = (counter_reg == 0);
  
  assign y = counter_reg;
  
  always @(posedge clk)
    counter_reg <= counter_next;
  
  always @* begin
    counter_next = counter_reg;
    
    if(reset)
      counter_next = 0;
    else if(inc && (counter_reg != MAX_VALUE))
      counter_next = counter_reg + 1;
    else if(dec && (counter_reg != 0))
      counter_next = counter_reg - 1;
  end
  
endmodule 