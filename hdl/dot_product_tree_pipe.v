`timescale 1ns / 1ps

module dot_product_tree_pipe
  #(
  parameter
  DATA_WIDTH = 16,
  LENGTH = 4,
  LOG2_LENGTH = 2
  )
  (
  input clk, input we,
  input wire [0:LENGTH*DATA_WIDTH-1] opA,
  input wire [0:LENGTH*DATA_WIDTH-1] opB,
  output wire [2*DATA_WIDTH+LOG2_LENGTH-1:0] result
  );
 
  wire [DATA_WIDTH-1:0] opA_vec [0:LENGTH-1];
  wire [DATA_WIDTH-1:0] opB_vec [0:LENGTH-1];

  // unpack the long inputs into an array
  genvar i;
  generate
  for(i = 0; i < LENGTH; i = i + 1) begin
  assign opA_vec[i] = opA[i*DATA_WIDTH:(i+1)*DATA_WIDTH-1];
  assign opB_vec[i] = opB[i*DATA_WIDTH:(i+1)*DATA_WIDTH-1];
  end
  endgenerate

  genvar x;
  generate
  for(x = 1; x <= LOG2_LENGTH; x = x + 1) begin: Ls
    // 2*DATA_WIDTH+1 is result length of the following operation; a * b + c * d
    // where a, b, c and d are all DATA_WIDTH long
    // after each new level of additions the length grows as the level number x
    wire [(2*DATA_WIDTH+x)-1:0] added [0:LENGTH/(2**x)-1];
    if(x == 1) begin
      // for the first level calculate the multiplications and first level additions 
      for(i = 0; i < LENGTH/(2**x); i = i + 1) begin
        mult_add_pipe #(.DATA_WIDTH(DATA_WIDTH))
          (clk, we, opA_vec[2*i], opA_vec[2*i+1], opB_vec[2*i], opB_vec[2*i+1], added[i]);
      end
    end
    else begin
      for(i = 0; i < LENGTH/(2**x); i = i + 1) begin
        add_pipe #(.DATA_WIDTH(2*DATA_WIDTH+x-1))
          (clk, we, Ls[x-1].added[2*i], Ls[x-1].added[2*i+1], added[i]);
      end
      
      if(x == LOG2_LENGTH) begin
        assign result = added[0];
      end
    end
  end
  endgenerate
 
endmodule
