from interface import *
import numpy as np

x = Interface()

# the matrices are just after each other, B after A
# memory will be organized as follows :

# memory a :
# matrix A
# matrix B
# instruction code
# address of matrix A
# address of matrix B
# address of matrix C in memory b

# memory b :
# matrix C

matrix_size = 8

A = np.random.randint(0, 15, size=(matrix_size, matrix_size))
B = np.random.randint(0, 15, size=(matrix_size, matrix_size))
C = np.zeros(shape=(matrix_size, matrix_size), dtype=np.int64)

A_address = 0
B_address = A_address + np.prod(A.shape) # B matrix is just after A matrix
C_address = 0

# codes for different matrix multiplications
# 1 - A * B
# 3 - A * BT
# 5 - AT * B
# 7 - AT * BT

operation = 5
alpha = 1
beta = 1

cpu_result = list(A.transpose().dot(B).ravel())

memory_a_map = []
memory_a_map += list(A.ravel())
memory_a_map += list(B.ravel())
memory_a_map += [operation, A_address, B_address, C_address, alpha, beta]

instr_addr = A_address + np.prod(A.shape) + np.prod(B.shape)

memory_b_map = []
memory_b_map += list(C.ravel())

# write to memory a
x.wr(0, A_address, memory_a_map)

# write to memory b
x.wr(1, C_address, memory_b_map)

def ex():
  x.ex(instr_addr)

def test():
  fpga_result = x.rd(1, C_address, len(memory_b_map))
  return fpga_result == cpu_result
  
if __name__ == '__main__':
  ex()
  time.sleep(0.5)
  assert test()
  #print(test())
  
