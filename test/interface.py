import serial
import time

class Interface(object):
  BYTE_WIDTH = 8
  BYTE_MASK = 0xff
  MEMORY_WIDTH_IN_BYTES = 2
  ADDRESS_WIDTH = 12
  DATA_WIDTH = BYTE_WIDTH * MEMORY_WIDTH_IN_BYTES
  CODE_WIDTH = 4
  MAX_ADDRESS = pow(2, ADDRESS_WIDTH) - 1
  MAX_DATA = pow(2, DATA_WIDTH) - 1
  
  # this is the counter size on the device
  # it keeps track how many bytes where transfered
  # if more bytes are required the transmission must happen in batches
  MAX_SENT = pow(2, BYTE_WIDTH-1)
  
  WRITE_CODE = 0x00
  READ_CODE = 0x01
  EXECUTE_CODE = 0x02
    
  def __init__(self, port='/dev/ttyUSB1', baudrate=9600):
    self.serial_port = serial.Serial(
    port=port,
    baudrate=baudrate,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout = 1.0)
  
  def write_port(self, x):
    self.serial_port.write(bytes(x))
  
  def read_port(self, num_requested):
    return self.serial_port.read(num_requested)
  
  def drain_port(self):
    out = []
    
    while self.serial_port.inWaiting() > 0:
      out.append(self.serial_port.read(1))
      
    if len(out) > 0:
      print(out)
    else:
      print('no data')
  
  def write(self, memory, address, data):
    """Writes starting from specified address bytes stored in data list.
    A fixed number of bytes received by the device make a whole memory word.
    """
    
    num_bytes_to_send = len(data)
    
    code = self.BYTE_MASK & ((self.WRITE_CODE << 1) | (memory & 1))
    
    to_send = [code, 
    self.BYTE_MASK & address, 
    self.BYTE_MASK & (address >> self.BYTE_WIDTH),
    num_bytes_to_send]
    
    to_send += data
    #print(bytes(to_send))
    
    self.serial_port.write(bytes(to_send))
    
  def read(self, memory, address, num_requested, delay_before_read=0.01):
    """Reads starting from specified address num_requested bytes.
    Every few bytes received make a whole memory word on the device memory.
    """
    
    code = self.BYTE_MASK & ((self.READ_CODE << 1) | (memory & 1))
    
    to_send = [code, 
    self.BYTE_MASK & address, 
    self.BYTE_MASK & (address >> self.BYTE_WIDTH),
    num_requested]
    
    self.serial_port.write(bytes(to_send))
    #print(bytes(to_send))
    time.sleep(delay_before_read)
    return self.serial_port.read(num_requested)
    
  def write_map(self, memory, address, memory_map):
    """Writes given memory_map on to the device.
    The memory_map list should contain the memory entries for the device.
    Before being send, each memory entry is divided in to bytes that lend themselves
    to transmission.
    """
    
    to_send = []
    for memory_element in memory_map:
      to_send.append(self.BYTE_MASK & memory_element)
      to_send.append(self.BYTE_MASK & (memory_element >> self.BYTE_WIDTH))
    
    index = 0
    while index < len(to_send):
      self.write(memory, address, to_send[index:min(index + self.MAX_SENT, len(to_send))])
      time.sleep(0.01)
      index += self.MAX_SENT
      address += int(self.MAX_SENT / self.MEMORY_WIDTH_IN_BYTES)
      
  def read_map(self, memory, address, num_requested):
    """Reads starting from given address the memory cells and writes them to a list.
    The memory cells are sent in seperate bytes requiring reconstruction.
    """
    
    received = []
    
    # num_requested is the number of memory cells we whant to receive
    # since the transmission happens in bytes we need to multiply the
    # requested number by the number of bytes in each memory cell
    num_requested *= self.MEMORY_WIDTH_IN_BYTES
    
    num_received = 0
    while num_received < num_requested:
      fragment = self.read(memory, address, min(self.MAX_SENT, num_requested - num_received))
      address += int(len(fragment) / self.MEMORY_WIDTH_IN_BYTES)
      num_received += len(fragment)
      received += fragment
      
    recovered = []
    for it_i in range(0, len(received), 2):
      recovered.append((received[it_i+1] << self.BYTE_WIDTH) | received[it_i])
      #print(it_i, received[it_i+1], received[it_i])
      
    return recovered
    
  def zombie_read_map(self, memory, address, num_requested):
    """This is a modified version of read_map that accounts for the
    error (of unknown source) in transmission where the third sent byte is always
    the same as the first sent byte.
    """
    
    recovered = self.read_map(memory, address, num_requested)
    repeat_interval = int(self.MAX_SENT / self.MEMORY_WIDTH_IN_BYTES)
    wrong_data_index = 1
    
    while wrong_data_index < num_requested:
      tmp = self.read_map(memory, address + wrong_data_index, 1)[0]
      recovered[wrong_data_index] = tmp
      wrong_data_index += repeat_interval
      
    return recovered     
    
  def slow_read_map(self, memory, address, num_requested):
    received = []
    tmp = []
    for it_i in range(num_requested):
        tmp = self.read(memory, address, self.MEMORY_WIDTH_IN_BYTES, 0.001)
        address += 1
        received.append((tmp[1] << self.BYTE_WIDTH) | tmp[0])
        
    return received
    
  def execute(self, address):
    # instructions are only executed from memory 0
    code = self.BYTE_MASK & (self.EXECUTE_CODE << 1)
    
    to_send = [code, 
    self.BYTE_MASK & address, 
    self.BYTE_MASK & (address >> self.BYTE_WIDTH),
    0] # this is a dummy data, expected by the 
    
    self.serial_port.write(bytes(to_send))
    
  def wr(self, memory, address, memory_map):
    self.write_map(memory, address, memory_map)
    
  def rd(self, memory, address, num_requested):
    return self.zombie_read_map(memory, address, num_requested)
    
  def ex(self, address):
    self.execute(address)
    
    
