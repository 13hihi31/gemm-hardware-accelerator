from interface import *
import numpy as np
import time

x = Interface(port='/dev/ttyUSB2')

# the matrices are just after each other, B after A
# memory will be organized as follows :

# memory a :
# matrix A00
# matrix A01
# matrix A10
# matrix A11
# matrix B00
# matrix B01
# matrix B10
# matrix B11
# instruction code
# address of matrix A00
# address of matrix B00
# address of matrix C00 in memory b
# instruction code
# address of matrix A01
# address of matrix B10
# address of matrix C00 in memory b
# instruction code
# address of matrix A00
# address of matrix B01
# address of matrix C01 in memory b
# instruction code
# address of matrix A01
# address of matrix B11
# address of matrix C01 in memory b
# instruction code
# address of matrix A10
# address of matrix B00
# address of matrix C10 in memory b
# instruction code
# address of matrix A11
# address of matrix B10
# address of matrix C10 in memory b
# instruction code
# address of matrix A10
# address of matrix B01
# address of matrix C11 in memory b
# instruction code
# address of matrix A11
# address of matrix B11
# address of matrix C11 in memory b

# memory b :
# matrix C00
# matrix C01
# matrix C10
# matrix C11

memory_size = 2**12

matrix_size = 16

A = np.random.randint(0, 15, size=(matrix_size, matrix_size))
B = np.random.randint(0, 15, size=(matrix_size, matrix_size))
C = np.zeros(shape=(matrix_size, matrix_size), dtype=np.int64)

small_matrix_size = matrix_size // 2;
small_matrix_num_elem = small_matrix_size**2;

A00_address = 0
A01_address = A00_address + small_matrix_num_elem
A10_address = A01_address + small_matrix_num_elem
A11_address = A10_address + small_matrix_num_elem

B00_address = A11_address + small_matrix_num_elem
B01_address = B00_address + small_matrix_num_elem
B10_address = B01_address + small_matrix_num_elem
B11_address = B10_address + small_matrix_num_elem

C00_address = 0
C01_address = C00_address + small_matrix_num_elem
C10_address = C01_address + small_matrix_num_elem
C11_address = C10_address + small_matrix_num_elem

s = small_matrix_size

A00 = A[0:s, 0:s]
A01 = A[0:s, s:2*s]
A10 = A[s:2*s, 0:s]
A11 = A[s:2*s, s:2*s]

B00 = B[0:s, 0:s]
B01 = B[0:s, s:2*s]
B10 = B[s:2*s, 0:s]
B11 = B[s:2*s, s:2*s]

C00 = C[0:s, 0:s]
C01 = C[0:s, s:2*s]
C10 = C[s:2*s, 0:s]
C11 = C[s:2*s, s:2*s]

# codes for different matrix multiplications
# 1 - A * B
# 3 - A * BT
# 5 - AT * B
# 7 - AT * BT

operation = 1
alpha = 1
beta = 1

cpu_result = alpha * A.dot(B) + beta * C

cpu_result_list = list(cpu_result[0:s, 0:s].ravel())
cpu_result_list += list(cpu_result[0:s, s:2*s].ravel())
cpu_result_list += list(cpu_result[s:2*s, 0:s].ravel())
cpu_result_list += list(cpu_result[s:2*s, s:2*s].ravel())

memory_a_map = []
memory_a_map += list(A00.ravel())
memory_a_map += list(A01.ravel())
memory_a_map += list(A10.ravel())
memory_a_map += list(A11.ravel())
memory_a_map += list(B00.ravel())
memory_a_map += list(B01.ravel())
memory_a_map += list(B10.ravel())
memory_a_map += list(B11.ravel())
memory_a_map += [operation, A00_address, B00_address, C00_address, alpha, beta]
memory_a_map += [operation, A01_address, B10_address, C00_address, alpha, beta]
memory_a_map += [operation, A00_address, B01_address, C01_address, alpha, beta]
memory_a_map += [operation, A01_address, B11_address, C01_address, alpha, beta]
memory_a_map += [operation, A10_address, B00_address, C10_address, alpha, beta]
memory_a_map += [operation, A11_address, B10_address, C10_address, alpha, beta]
memory_a_map += [operation, A10_address, B01_address, C11_address, alpha, beta]
memory_a_map += [operation, A11_address, B11_address, C11_address, alpha, beta]

if len(memory_a_map) > memory_size:
  print('ran out of memory')

instr_addr1 = A00_address + np.prod(A.shape) + np.prod(B.shape)
instr_addr2 = instr_addr1 + 6
instr_addr3 = instr_addr2 + 6
instr_addr4 = instr_addr3 + 6
instr_addr5 = instr_addr4 + 6
instr_addr6 = instr_addr5 + 6
instr_addr7 = instr_addr6 + 6
instr_addr8 = instr_addr7 + 6

memory_b_map = []
memory_b_map += list(C00.ravel())
memory_b_map += list(C01.ravel())
memory_b_map += list(C10.ravel())
memory_b_map += list(C11.ravel())

# write to memory a
x.wr(0, A00_address, memory_a_map)

# write to memory b
x.wr(1, C00_address, memory_b_map)

def ex():
  x.ex(instr_addr1)
  time.sleep(0.2)
  x.ex(instr_addr2)
  time.sleep(0.2)
  x.ex(instr_addr3)
  time.sleep(0.2)
  x.ex(instr_addr4)
  time.sleep(0.2)
  x.ex(instr_addr5)
  time.sleep(0.2)
  x.ex(instr_addr6)
  time.sleep(0.2)
  x.ex(instr_addr7)
  time.sleep(0.2)
  x.ex(instr_addr8)

def test():
  fpga_result = x.rd(1, C00_address, len(memory_b_map))
  return fpga_result == cpu_result_list
  
if __name__ == '__main__':
  ex()
  time.sleep(0.5)
  assert test()
  #print(test())
  
